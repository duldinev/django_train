from django.contrib import admin
from .models import Person, Pictures


admin.site.register(Person)
admin.site.register(Pictures)
