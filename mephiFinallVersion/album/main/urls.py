from django.urls import path
from .views import main, add_new, open_or_reg, RegisterUser, LoginUser, logout_user, help_window, galary, get_slaid, get_measures

urlpatterns = [

    path('', open_or_reg, name='open_or_reg'),
    path('reg/', RegisterUser.as_view(), name='reg_user'),
    path('main/', main, name='main'),
    path('adds/', add_new, name='adds'),

    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', logout_user, name='logout'),

    path('helps/', help_window, name='helps'),

    path('galarys/', galary, name='gals'),
    path('slaider/', get_slaid, name='slaider'),
    path('measures/', get_measures, name='measures')

]
