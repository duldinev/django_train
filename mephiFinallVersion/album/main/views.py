from django.contrib.auth import logout, login
from django.contrib.auth.views import LoginView
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse_lazy

from django.views.generic import CreateView
from django.contrib.auth.forms import UserCreationForm

from .models import Person, Pictures
from .forms import *


class RegisterUser(CreateView):
    form_class = RegisterUserForm
    template_name = 'main/register_form.html'
    success_url = reverse_lazy('open_or_reg')

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('main')


class LoginUser(LoginView):
    form_class = LoginUserForm
    template_name = 'main/login_form.html'

    def get_success_url(self):
        return reverse_lazy('main')


def logout_user(request):
    logout(request)
    return redirect('open_or_reg')


def open_or_reg(request):
    return render(request, 'main/open_or_reg_form.html')


def main(request):
    print(request.user.id)
    person_all = Person.objects.all()[0:4]
    pictures_all = Pictures.objects.filter(my_pictures=request.user.id)
    empty_gal = ''
    picture = None

    count = len(pictures_all)
    if count == 0:
        empty_gal = 'Вы до сих пор не внесли фото'
    elif count < 6:
        picture = pictures_all
    else:
        picture = pictures_all[count - 6:]

    data = {
        'person': person_all,
        'picture': picture,
        'empty_gal' : empty_gal,
    }
    return render(request, 'main/main.html', data)


def add_new(request):
    form = AddPicture()

    if request.POST:

        # remember old state
        _mutable = request.POST._mutable
        # set to mutable
        request.POST._mutable = True
        # сhange the values you want
        request.POST['my_pictures'] = str(request.user.id)
        # set mutable flag back
        request.POST._mutable = _mutable

        form = AddPicture(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            return redirect('main')

    data = {
        'form': form
    }
    return render(request, 'main/addPicture.html', data)


def help_window(request):
    return render(request, 'main/help.html')


def galary(request):

    obj = Pictures.objects.filter(my_pictures=request.user.id)
    data = {'obj' : obj}
    return render(request, 'main/galary_user.html', data)


def get_slaid(request):
    obj = Pictures.objects.filter(my_pictures=request.user.id)
    count = len(obj)
    mess = ""

    if count < 4:
        mess += f'У вас мало фото всего: {count} загрузите ещё {4 - count} фото для более быстрой работы'
    data = {
        'pic' : obj,
        'mess' : mess,
    }
    return render(request, 'main/slaider.html', data)


def get_measures(request):
    try:
        obj = Pictures.objects.filter(my_pictures=request.user.id)
        count = len(obj)
        obj = obj[count-1:count]
        data = {'obj': obj}

        return render(request, 'main/measures.html', data)
    except:
        return HttpResponse('Добавте хоть одно фото')