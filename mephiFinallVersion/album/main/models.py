from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone




class Person(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя')
    age = models.IntegerField()
    image_p = models.ImageField(upload_to="photos", verbose_name="Фото", null=True, blank=True)

    def __str__(self):
        return self.name


class Pictures(models.Model):
    picture_name = models.ImageField(upload_to='pictures', default='default.jpg')
    info_dop = models.TextField(null=True, blank=True)
    date_time = models.DateTimeField(default=timezone.now)
    my_pictures = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return str(self.pk)