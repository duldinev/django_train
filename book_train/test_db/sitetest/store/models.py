from django.db import models


# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, verbose_name='User_name')
    age = models.IntegerField()
    meta_info = models.TextField()

    def __str__(self):
        return self.name
