from .models import Article
from django.forms import ModelForm, TextInput, DateTimeInput, Textarea


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'anons', 'full_text', 'date']

        widgets = {
            'title': TextInput(attrs={
                'placeholder': "Название статьи"
            }),
            'anons': TextInput(attrs={
                'placeholder': "Анонс статьи"
            }),
            'date': DateTimeInput(attrs={
                'placeholder': 'Врема добавления'
            }),
            'full_text': Textarea(attrs={
                'placeholder': 'Текст статьи'
            })
        }
