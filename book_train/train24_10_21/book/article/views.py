from django.shortcuts import render
from django.views.generic.edit import CreateView
from .forms import RubricForm
from .models import Rubric


def start(request):
    return render(request, 'article/start.html')


class RubricReg(CreateView):
    template_name = 'article/registration_rubruc.html'
    form_class = RubricForm
    success_url = '/'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context["rubrics"] = Rubric.objects.all()
        return context