from django.contrib import admin
from .models import Author, Rubric

admin.site.register(Author)
admin.site.register(Rubric)
