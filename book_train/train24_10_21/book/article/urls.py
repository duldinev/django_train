from django.urls import path
from .views import start, RubricReg

urlpatterns = [
    path('', start, name='start_page'),
    path('reg/', RubricReg.as_view(), name='add'),

]
