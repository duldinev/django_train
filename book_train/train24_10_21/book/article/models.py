from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя автора')
    age = models.IntegerField()
    count = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class Rubric(models.Model):
    intro = models.CharField(max_length=100, verbose_name='Название статьи')
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    def __str__(self):
        return self.intro
