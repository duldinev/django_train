from django.forms import ModelForm
from .models import Rubric


class RubricForm(ModelForm):
    class Meta:
        model = Rubric
        fields = ['intro', 'author']
