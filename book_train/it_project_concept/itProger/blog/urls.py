from django.urls import path
from .views import home, contact

urlpatterns = [
    path('', home, name='blog-home'),
    path('contact/', contact, name='blog-contact')
]
