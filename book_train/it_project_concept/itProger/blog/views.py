from django.shortcuts import render
from django.http import HttpResponse
from .models import News

# Create your views here.
def home(request):
    data = {
        'news' : News.objects.all()
    }
    return render(request, 'blog/home.html', data)

def contact(request):
    return HttpResponse('Page contact')

def main(request):
    return render(request, 'blog/main.html')