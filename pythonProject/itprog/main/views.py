from django.shortcuts import render


# Create your views here.

def index(request):
    return render(request, 'main/indexes.html', {'title': 'Main page'})


def about(request):
    data= {
        'values' : ['first', 1, 'seconds'],
        'obj' : {
            'car' : 'new',
            'tost' : 'old',
            'ramp' : 'mid'
                 },
        'age' : 32
    }

    return render(request, 'main/about.html', data)
