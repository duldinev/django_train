from django.db import models


# Create your models here.

class Articles(models.Model):
    title = models.CharField('name', max_length=50, default='top')
    anons = models.CharField('annons', max_length=250)
    full_text = models.TextField('article')
    date = models.DateTimeField('date_time')

    def __str__(self):
        return self.title
